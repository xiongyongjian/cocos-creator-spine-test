/*
 * @Author: bob.xiong 
 * @Date: 2021-08-30 11:18:15 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 11:29:44
 */

/** 显示数据的下标范围 */
export class CellIndexRange {
    min: number = -1;
    max: number = -1;

    constructor(min: number = -1, max: number = -1) {
        this.min = min;
        this.max = max;
    }

    public reset(): void {
        this.min = this.max = -1;
    }
}