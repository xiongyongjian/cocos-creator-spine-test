/*
 * @Author: bob.xiong 
 * @Date: 2021-08-26 10:13:27 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 09:10:07
 */

export class CellViewConfig {

    public top: number;
    public bottom: number;
    public left: number;
    public right: number;
    public spacingX: number;
    public spacingY: number;
    public rowCount: number;
    public layers: cc.Node[];
    public content: cc.Node;

    constructor(top: number, bottom: number, left: number, right: number, spacingX: number, spacingY: number, rowCount: number, layers: cc.Node[], content: cc.Node) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.spacingX = spacingX;
        this.spacingY = spacingY;
        this.rowCount = rowCount;
        this.layers = layers;
        this.content = content;
    }

}