
# CellView组件

## 功能介绍
- 列表布局
- Cell复用
- 分层渲染
- 跳转到指定index

## 目录结构
- demo: 样例目录 - 仅作参考，可以删除
- data: 数据结构
- prefab: 组件预制体
- script: 脚本目录

## 使用介绍

### 一、CellView组件
- 1、将prefab目录下的预制体拖入场景中
- 2、在属性面板配置参数
- 3、调用CellView.init初始化，参数1：列表中的数据，参数2：要跳转到的数据下标

### 二、Cell组件
- 1、创建自定义的Cell预制体【也可直接复制demo的预制体修改】
- 2、创建自定义的Cell脚本，必须继承于CellBase，并实现两个抽象接口【也可直接复制demo的脚本修改】
- 3、将自定义的Cell脚本添加到Cell预制体的根节点上
- 4、替换掉CellView的属性面板中配置的预制体

### 三、分层渲染
- 1、Cell预制体中的根节点下创建layer节点，将能合批的节点放到同一个layer下面，并将layer拖入Cell属性面板的分层属性中
- 2、在CellView的属性面板中对应设置分层数，必须与Cell预制体中的分层数相同
- 3、运行时会抽离预制体中的layer到CellView的layer中，实现分层合批

### 四、参考
- 1、参照demo目录下的样例

## 注意事项

### 一、spine动画直接放进预制体无法合批 - 解决办法
- 1、可以代码创建spine组件，并添加到Cell的分层节点中
- 2、设置spine的enableBatch为true
- 3、设置缓存模式setAnimationCacheMode为sp.Skeleton.AnimationCacheMode.PRIVATE_CACHE 或者 sp.Skeleton.AnimationCacheMode.SHARED_CACHE

### 二、Label在web环境下不合批 - 解决办法
- 1、将Label放到Cell的分层节点中
- 2、Label设置CacheMode设置成Char或者BITMAP
- 也可以用位图字体代替，实现合批