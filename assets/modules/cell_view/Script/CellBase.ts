/*
 * @Author: bob.xiong 
 * @Date: 2021-08-26 09:05:06 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 20:11:03
 */

import { CellViewConfig } from "../data/CellViewConfig";


const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class CellBase extends cc.Component {

    @property({ type: [cc.Node], displayName: "分层渲染", tooltip: "分层数请务必与CellVeiw的分层数相同，能合批的组件放在同一层，实现分层合批渲染。layer节点的父节点必须是预制体的根节点" })
    layers: cc.Node[] = [];

    private _layerPositions: cc.Vec3[] = [];

    /** 唯一标识 */
    protected _id: number = -1;

    public index: number = -1;

    private _config: CellViewConfig = null;

    /** 数据是根据下标从总数据的数组中取到的数据*/
    private _data: any = null;

    //----------------------------------------------------------------

    /** 抽象接口: 更新Cell
     * @param index 下标
     * @param data cell对应的数据 - CellView初始化时传入的数组中通过index获取的数据
     */
    protected abstract updateCell(index: number, data: any): void;

    /** 抽象接口: 重置Cell */
    protected abstract resetCell(): void;

    //----------------------------------------------------------------

    /** 初始化
     * @param config cellview的配置参数
     * @param id cell的唯一标识
     */
    public init(config: CellViewConfig, id: number): void {
        this._config = config;
        this._id = id;
        this.$initLayer();
    }

    /**更新数据和位置
     * @param index 数据下标
     * @param datas 总数据
    */
    public updateData(index: number, datas: any[]): void {
        if (this.index == index) return;
        this.$updateOffset(index, index >= datas.length);
        this.index = index;
        this._data = datas[index];
        this.updateCell && this.updateCell(index, this._data);
    }

    /** 重置*/
    public reset(): void {
        this.index = -1;
        this._data = null;
        this.resetCell && this.resetCell();
    }

    /** 更新位置
     * @param index 数据下标
     * @param hasData 是否有数据
     */
    private $updateOffset(index: number, hasData: boolean): void {
        if (hasData) {
            this.node.parent.x = -cc.winSize.width;
            this.node.parent.y = -cc.winSize.height;
        } else {
            index++;
            if (!this._config) return console.error(`[ListItemBase]: updateOffset item 未初始化`);
            let config: CellViewConfig = this._config;
            let indexY: number = Math.ceil(index / config.rowCount);
            let indexX: number = index % config.rowCount;
            indexX == 0 ? indexX = config.rowCount : null;
            let x: number = config.left + (this.node.width + config.spacingX) * (indexX - 1);
            let y: number = config.top + (this.node.height + config.spacingY) * (indexY - 1);
            this.node.parent.x = x;
            this.node.parent.y = -y;
        }
        this.$updateLayers();
    }

    /** 初始化分层 */
    private $initLayer(): void {
        let config: CellViewConfig = this._config;
        if (this.layers.length == 0 || config.layers.length == 0) return;

        let cellLayer: cc.Node = null;
        let contentLayer: cc.Node = null;
        let gPos: cc.Vec3 = null;
        let layerPos: cc.Vec3 = null;
        for (let i = 0; i < this.layers.length; i++) {
            cellLayer = this.layers[i];
            if (cellLayer.getComponent(cc.Widget)) {
                cellLayer.removeComponent(cc.Widget);
            }
            layerPos = cellLayer.position;
            this._layerPositions.push(layerPos);
            contentLayer = (i < config.layers.length) ? config.layers[i] : config.layers[config.layers.length - 1];
            gPos = this.node.convertToWorldSpaceAR(layerPos);
            cellLayer.parent = contentLayer;
            cellLayer.position = contentLayer.parent.convertToNodeSpaceAR(gPos);
        }
    }

    /** 更新其他层的位置 */
    private $updateLayers(): void {
        if (this.layers.length > 0 && this._config.layers.length > 0) {
            let cellLayer: cc.Node = null;
            let gPos: cc.Vec3 = null;
            let layerPos: cc.Vec3 = null;
            for (let i = 0; i < this.layers.length; i++) {
                cellLayer = this.layers[i];
                layerPos = this._layerPositions[i];
                gPos = this.node.convertToWorldSpaceAR(layerPos);
                cellLayer.position = cellLayer.parent.convertToNodeSpaceAR(gPos);
            }
        }
    }

}
