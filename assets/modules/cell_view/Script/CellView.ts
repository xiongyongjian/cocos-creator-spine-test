/*
 * @Author: bob.xiong 
 * @Date: 2021-08-26 09:04:39 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 20:11:27
 */

import { CellIndexRange } from "../data/CellIndexRange";
import { CellViewConfig } from "../data/CellViewConfig";
import CellBase from "./CellBase";
import CellMgr from "./CellMgr";


const { ccclass, property } = cc._decorator;

@ccclass
export default class CellView extends cc.ScrollView {

    @property(cc.Float)
    top: number = 10;


    @property(cc.Float)
    bottom: number = 10;

    @property(cc.Float)
    left: number = 10;

    @property(cc.Float)
    right: number = 10;

    @property(cc.Float)
    spacingX: number = 20;

    @property(cc.Float)
    spacingY: number = 20;

    @property({ type: cc.Integer, displayName: '列数' })
    columnsCount: number = 3;

    @property({ type: cc.Integer, displayName: "分层渲染", tooltip: "分层数请务必与CellBase中设置的层数相同，能合批的组件放在同一层，实现分层合批渲染" })
    layerCount: number = 0;

    @property(cc.Prefab)
    cellPrefab: cc.Prefab = null;

    /** 层 */
    private _layers: cc.Node[] = [];
    /** 列表配置 */
    private _config: CellViewConfig = null;

    private _cellWidth: number = 0;
    private _cellHeight: number = 0;
    private _cellMgr: CellMgr = null;

    private _offset: cc.Vec2 = cc.v2(-1, -1);
    private get offset() { return this._offset; }
    private set offset(v: cc.Vec2) {
        if (v.y > this.content.height - this.node.height) {
            v.y = this.content.height - this.node.height;
        } else if (v.y < 0) {
            v.y = 0;
        }
        if (this._offset.y == v.y) return;
        this._offset = v;
        this.$updateOffset();
    }

    /** 滑动前后下标范围与显示的范围是否相同 */
    private _isSameRange: boolean = true;

    /** 下标范围 */
    private _indexRange: CellIndexRange = new CellIndexRange();

    /** 滑动时不变的下标范围 */
    private _staticIndexRange = new CellIndexRange();

    /** 数据 */
    private _datas: any[] = [];

    /** 是否刷新 
     * - true: 刷新所有cell 
     * - false: 只刷新变动的cell 
     */
    private _isRefresh: boolean = false;

    private _isPrefabEnable: boolean = false;

    onLoad(): void {
        this.$initLayers();
        this._config = new CellViewConfig(this.top, this.bottom, this.left, this.right, this.spacingX, this.spacingY, this.columnsCount, this._layers, this.content);
        if (!this.$checkPrefabEnable()) return;
        this._cellWidth = this.cellPrefab.data.width;
        this._cellHeight = this.cellPrefab.data.height;
        this.node.width = this._cellWidth * this.columnsCount + this.spacingX * (this.columnsCount - 1) + this.left + this.right;
        this.content.width = this.node.width;
        let itemCount: number = Math.ceil(this.node.height / (this._cellHeight + this.spacingY)) * this.columnsCount;
        this._cellMgr = new CellMgr(this.cellPrefab, itemCount, this._config);
    }

    onEnable(): void {
        super.onEnable && super.onEnable();
        this.node.on('scrolling', this.$onScrolling, this);
    }

    onDisable(): void {
        super.onDisable && super.onDisable();
        this.node.off('scrolling', this.$onScrolling, this);
    }

    /** 初始化
     * @param dataList 列表中的数据
     * @param index 跳转到指定的cell
     */
    public init(dataList: Array<any>, index: number = 0): void {
        if (!dataList || dataList.length == 0) return console.error(`[ListView]: initList 初始化列表传入数据错误`);
        this._isRefresh = true;
        this._datas = dataList;
        let len: number = dataList.length;
        let yCount: number = Math.ceil(len / this.columnsCount);
        this.content.height = this._cellHeight * yCount + this.spacingY * (yCount - 1) + this.top + this.bottom;

        //刷新cell
        this.offset = cc.v2(0, 0);
        if (index > 0) {
            this.scheduleOnce(() => {
                //跳转到指定的cell位置
                this.scrollToIndex(index);
            }, 0);
        }
    }

    /** 跳转到指定位置
     * @param index 要跳转的数据下标
     */
    public scrollToIndex(index: number): void {
        let rowIdx: number = Math.ceil((index + 1) / this.columnsCount);
        let y: number = (rowIdx - 1) * (this._cellHeight + this.spacingY);
        if (y > (this.content.height - this.node.height)) {
            y = this.content.height - this.node.height;
        }
        this._isRefresh = true;
        this.scrollToOffset(cc.v2(0, y));
        this.offset = cc.v2(0, y);
    }

    /** 初始化分层 */
    private $initLayers(): void {
        if (this.layerCount > 0) {
            let layer: cc.Node;
            for (let i = 0; i < this.layerCount; i++) {
                layer = new cc.Node(`contentLayer_${i + 1}`);
                layer.parent = this.content;
                layer.x = layer.y = 0;
                layer.width = layer.height = 0;
                layer.zIndex = i + 1;
                this._layers.push(layer);
            }
        }
    }

    /** 滚动事件 */
    private $onScrolling(): void {
        this.offset = this.getScrollOffset();
    }

    /** 更新偏移值 */
    private $updateOffset(): void {
        if (!this.$checkPrefabEnable()) return;
        this.$updateIndexRange();
        if (this._isSameRange) return;
        this.$updateCells();
    }

    /** 更新下标范围 */
    private $updateIndexRange() {
        let range: CellIndexRange = new CellIndexRange();
        const minY: number = this.offset.y;
        const maxY: number = minY + this.node.height;
        let rowHeight: number = this._cellHeight + this.spacingY;
        let minRowIdx: number = Math.ceil((minY - this.top) / rowHeight);
        let maxRowIdx: number = Math.ceil((maxY - this.top) / rowHeight);
        range.min = (minRowIdx - 1) * this.columnsCount;
        range.min < 0 ? range.min = 0 : null;
        range.max = maxRowIdx * this.columnsCount - 1;
        if (range.max > this._datas.length - 1) {
            range.max = this._datas.length - 1;
        }

        this._isSameRange = false;
        if (this._indexRange.max == 0 && this._indexRange.min == 0) {
            this._staticIndexRange.min = -1;
            this._staticIndexRange.max = -1;

        } else if (range.min == this._indexRange.min && range.max == this._indexRange.max) {
            this._staticIndexRange.min = range.min;
            this._staticIndexRange.max = range.max;
            return this._isSameRange = true;

        } else if (range.min > this._indexRange.min) {
            this._staticIndexRange.min = range.min;
            this._staticIndexRange.max = this._indexRange.max;

        } else if (range.min < this._indexRange.min) {
            this._staticIndexRange.min = this._indexRange.min;
            this._staticIndexRange.max = range.max;
        }
        this._indexRange = range;

    }

    /** 更新cell数据和位置 */
    private $updateCells(): void {
        let cellNode: cc.Node = null;
        if (this._isRefresh) {
            this._staticIndexRange = new CellIndexRange();
        }
        this._cellMgr.updatePoolData(this._indexRange, this._isRefresh);
        for (let i = this._indexRange.min; i <= this._indexRange.max; i++) {
            if (i >= this._staticIndexRange.min && i <= this._staticIndexRange.max) continue;
            cellNode = this._cellMgr.getCell();
            cellNode.getChildByName('cell_base').getComponent(CellBase).updateData(i, this._datas);
        }
        this._isRefresh ? this._isRefresh = false : null;
    }

    private $checkPrefabEnable(): boolean {
        if (this._isPrefabEnable) return this._isPrefabEnable;
        this._isPrefabEnable = false;
        if (!this.cellPrefab || !this.cellPrefab.data) {
            console.error('[ListView]: onload 预制体设置错误');
            return false;
        }
        if (!this.cellPrefab.data.getComponent(CellBase)) {
            console.error('[ListView]: onload 预制体根节点不存在CellBase的组件');
            return false;
        }
        this._isPrefabEnable = true;
        return true;
    }
}
