/*
 * @Author: bob.xiong 
 * @Date: 2021-08-27 17:46:03 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 20:09:26
 */

import { CellIndexRange } from "../data/CellIndexRange";
import { CellViewConfig } from "../data/CellViewConfig";
import CellBase from "./CellBase";

const { ccclass, property } = cc._decorator;

export default class CellMgr {

    private _prefab: cc.Prefab = null;

    /** 使用中的item */
    private _usePool: cc.Node[] = [];

    /** 空闲的item */
    private _idlePool: cc.Node[] = [];

    /** 列表配置 */
    private _config: CellViewConfig = null;

    /** 标记item的id */
    private _id: number = 0;

    constructor(prefab: cc.Prefab, count: number, config: CellViewConfig) {
        this._prefab = prefab;
        this._config = config;
        this.$initIdlePool(count);
    }

    /** 获取item */
    public getCell(): cc.Node {
        let itemNode: cc.Node = null;
        if (this._idlePool.length > 0) {
            itemNode = this._idlePool.shift();
        } else {
            itemNode = this.$createCell();
        }
        this._usePool.push(itemNode);
        return itemNode;
    }

    /** 更新池子数据 */
    public updatePoolData(indexRange: CellIndexRange, isRefresh: boolean): void {
        let cellNode: cc.Node;
        let script: CellBase;
        let len: number = this._usePool.length;
        if (isRefresh) {
            for (let i = len - 1; i >= 0; i--) {
                cellNode = this._usePool[i];
                this._idlePool.push(cellNode);
                script = cellNode.getChildByName('cell_base').getComponent(CellBase);
                script.reset();
            }
            this._usePool.length = 0;
        } else {
            for (let i = len - 1; i >= 0; i--) {
                cellNode = this._usePool[i];
                script = cellNode.getChildByName('cell_base').getComponent(CellBase);
                if (script.index < indexRange.min || script.index > indexRange.max) {
                    this._usePool.splice(i, 1);
                    this._idlePool.push(cellNode);
                    script.reset();
                }
            }
        }
    }

    /** 初始化空闲池 */
    private $initIdlePool(count: number): void {
        let itemNode: cc.Node = null;
        for (let i = 0; i < count; i++) {
            itemNode = this.$createCell();
            this._idlePool.push(itemNode);
        }
    }

    /** 创建CellNode */
    private $createCell(): cc.Node {
        this._id++;
        let cellNode: cc.Node = new cc.Node(`cell_${this._id}`);
        let baseNode: cc.Node = cc.instantiate(this._prefab);

        cellNode.parent = this._config.content;
        cellNode.anchorX = 0;
        cellNode.anchorY = 1;
        cellNode.x = -cc.winSize.width;
        cellNode.y = -cc.winSize.height;
        cellNode.width = baseNode.width;
        cellNode.height = baseNode.height;

        baseNode.parent = cellNode;
        baseNode.x = baseNode.width * 0.5;
        baseNode.y = -baseNode.height * 0.5;
        baseNode.name = `cell_base`;
        baseNode.getComponent(CellBase).init(this._config, this._id);
        return cellNode;
    }

}
