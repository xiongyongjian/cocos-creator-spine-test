/*
 * @Author: bob.xiong 
 * @Date: 2021-08-30 14:55:40 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 20:09:24
 */

import CellView from "../Script/CellView";


const { ccclass, property } = cc._decorator;

@ccclass
export default class CellViewDemo extends cc.Component {

    @property(CellView)
    cellview: CellView = null;

    start(): void {
        let arr: number[] = [];
        let i: number = 0;
        for (i = 1; i <= 100; i++) {
            arr.push(i);
        }
        this.cellview.init(arr, 0);
    }

}
