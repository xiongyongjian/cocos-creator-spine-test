/*
 * @Author: bob.xiong 
 * @Date: 2021-08-30 18:00:27 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 18:14:42
 */

import CellBase from "../Script/CellBase";


const { ccclass, property } = cc._decorator;

/** 自定义cell实现类 */
@ccclass
export default class DemoCell extends CellBase {

    @property({ type: cc.Label, displayName: "测试的cell描述", tooltip: "正式使用时设置为空" })
    desLb: cc.Label = null;

    @property({ type: cc.Label, displayName: "测试的id标识", tooltip: "正式使用时设置为空" })
    idLb: cc.Label = null;

    protected updateCell(index: number, data: any): void {
        this.idLb.string = `${this._id}`;
        this.desLb.string = `${index + 1}`;
    }

    protected resetCell(): void {
        this.desLb.string = ``;
    }

}
