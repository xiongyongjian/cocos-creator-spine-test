/*
 * @Author: bob.xiong 
 * @Date: 2021-08-26 16:02:05 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-26 17:38:16
 */

export namespace role {

    export const SPINE_PATH: string = 'spine/racoon-normal';

    export enum ANI_NAMES {
        STANDBY = '001_daiji_01',
        STANDBY_2 = '001_daiji_02',
        JUMP_1 = '001_jump_01',
        JUMP_2 = '001_jump_01_L',
        JUMP_3 = '001_jump_01_R',
        JUMP_4 = '001_jump_02_L',
        JUMP_5 = '001_jump_02_R',
        JUMP_6 = '001_jump_snow',
        LOSE = '001_lose',
        WIN = '001_win',
        WIN_2 = '001_win_02',
        WIN_3 = '001_win_03',
    }

    export enum PART_NAMES {
        BODY = 'body',
        EAR = 'ear',
        LEG = 'leg',
        SHOES = 'shoes'
    }

    /** 每个部分的插槽配置 */
    export const PART_SLOT_NAMES_MAP: { [key: string]: string[] } = {
        'body': [
            'racoon_arm_L',
            'racoon_arm_L_out',
            'racoon_arm_R',
            'racoon_arm_R_out',
            'racoon_body',
            'racoon_weiba',
        ],
        'ear': [
            'racoon_ear_L',
            'racoon_ear_R',
        ],
        'leg': [
            'racoon_leg_L',
            'racoon_leg_R',
        ],
        'shoes': [
            'racoon_foot_L',
            'racoon_foot_R',
        ]
    }

    /** 纹理图集路径 */
    export const ATLAS_PATH_MAP: { [key: string]: string } = {
        'body': 'skin/body_',
        'ear': 'skin/ear_',
        'leg': 'skin/leg_',
        'shoes': 'skin/shoes_',
    }

}