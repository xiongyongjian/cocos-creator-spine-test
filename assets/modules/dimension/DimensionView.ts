/*
 * @Author: bob.xiong 
 * @Date: 2021-08-23 11:10:42 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-24 10:18:23
 */

const { ccclass, property } = cc._decorator;

/** 维度绘图脚本 */

@ccclass
export default class DimensionView extends cc.Graphics {

    private _targetPoins: cc.Vec2[] = [];

    private _degrees: number[] = [];

    private _vertexRadius: number = 2;

    private _lineWidth: number = 2;

    private _lineColor: cc.Color = null;

    private _vertexColor: cc.Color = null;

    /** 缓动时间 */
    private readonly _tweenTime: number = 3;

    /** 绘制图形
     * @param targetPoins 目标顶点位置
     * @param vertexRadius 顶点半径
     */
    public drawMap(targetPoins: cc.Vec2[], degrees: number[], vertexRadius: number, lineWidth: number,
        fillColor: cc.Color, lineColor: cc.Color, vertexColor: cc.Color): void {
        if (targetPoins.length < 3) return console.error('[DimensionView]: 绘制维度图参数错误 维度数小于3');
        this._targetPoins = targetPoins;
        this._degrees = degrees;
        this._vertexRadius = vertexRadius;
        this._lineWidth = lineWidth;
        this.fillColor = fillColor;
        this._lineColor = lineColor;
        this._vertexColor = vertexColor;

        let degree: number = 0;
        let point: cc.Vec2 = null;
        for (let i = 0; i < targetPoins.length; i++) {
            point = targetPoins[i];
            degree = degrees[i] || 0;
            degree > 0.99 ? degree = 0.99 : null;
            point.x *= degree;
            point.y *= degree;
        }
        this.$drawAnimation();
    }

    private $drawAnimation(): void {
        cc.Tween.stopAllByTarget(this.node);
        cc.tween(this.node)
            .set({ opacity: 200 })
            .to(this._tweenTime, { opacity: 255 }, {
                progress: (start: number, end: number, current: number, ratio: number) => {
                    this.$draw(this._targetPoins, ratio);
                    return start + (end - start) * ratio;
                },
                easing: 'expoOut'
            })
            .start();
    }

    /** 绘制 */
    private $draw(points: cc.Vec2[], ratio: number): void {
        this.clear();

        let startPoint: cc.Vec2 = points[0];
        let point: cc.Vec2 = null;

        this.moveTo(startPoint.x * ratio, startPoint.y * ratio);
        for (let i = 1; i < points.length; i++) {
            point = points[i];
            this.lineTo(point.x * ratio, point.y * ratio);
        }
        this.lineTo(startPoint.x * ratio, startPoint.y * ratio);

        this.fill();

        this.strokeColor = this._lineColor;
        this.stroke();

        this.$drawVertexs(points, ratio);
    }

    /** 绘制顶点 */
    private $drawVertexs(points: cc.Vec2[], ratio: number): void {
        let point: cc.Vec2 = null;
        for (let i = 0; i < points.length; i++) {
            point = points[i];
            this.circle(point.x * ratio, point.y * ratio, this._vertexRadius);
            this.strokeColor = cc.Color.GRAY;
            this.stroke();
        }
    }

}
