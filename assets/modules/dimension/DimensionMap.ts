/*
 * @Author: bob.xiong 
 * @Date: 2021-08-23 11:07:09 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-31 09:12:03
 */

import DimensionBg from "./DimensionBg";
import DimensionView from "./DimensionView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DimensionMap extends cc.Component {

    @property(DimensionBg)
    bgView: DimensionBg = null;

    @property(DimensionView)
    view: DimensionView = null;

    @property({ type: cc.Integer, displayName: "维度数" })
    pointCount: number = 5;

    @property({ type: cc.Integer, displayName: "每个维度等级数" })
    level: number = 5;

    @property({ type: cc.Float, displayName: "维度半径" })
    radius: number = 100;

    @property({ type: cc.Float, displayName: "顶点半径" })
    vertexRadius: number = 3;

    @property({ type: cc.Float, displayName: "画线宽度" })
    lineWidth: number = 3;

    @property(cc.Color)
    fillColor: cc.Color = null;

    @property(cc.Color)
    lineColor: cc.Color = null;

    @property(cc.Color)
    vertexColor: cc.Color = null;

    @property(cc.Color)
    bgColor: cc.Color = null;

    @property(cc.Color)
    bgLineColor: cc.Color = null;

    /** 初始化
     * @param degrees 每个维度的进度值（0-1）
     */
    public init(degrees: number[]): void {
        if (degrees.length < this.pointCount) {
            console.error(`[DimensionMap]: init 绘制维度图参数错误`);
            let len: number = degrees.length;
            for (let i = len; i < 5; i++) {
                degrees.push(0);
            }
        }
        let targetPoints: cc.Vec2[] = this.$generatePoints();
        this.$draw(this.$generatePoints(), degrees);
        this.bgView.drawBg(targetPoints, this.lineWidth, this.bgColor, this.bgLineColor, this.level);
    }

    /** 计算顶点坐标 */
    private $generatePoints(): cc.Vec2[] {
        let pointList: cc.Vec2[] = [];
        const r: number = this.radius;
        const count: number = this.pointCount;
        const deltaAngle: number = 360 / count;
        let angle: number = 0;
        let x: number, y: number;
        let point: cc.Vec2;
        if (count % 2 == 0) {
            angle = deltaAngle * 0.5;
            for (let i = 0; i < count; i++) {
                if (i != 0) {
                    angle += deltaAngle;
                }
                x = Math.sin(angle * Math.PI / 180) * r;
                y = Math.cos(angle * Math.PI / 180) * r;
                point = cc.v2(x, y);
                pointList.push(point);
            }
        } else {
            angle = 0;
            for (let i = 0; i < count; i++) {
                if (i != 0) {
                    angle += deltaAngle;
                }
                x = Math.sin(angle * Math.PI / 180) * r;
                y = Math.cos(angle * Math.PI / 180) * r;
                console.log(angle);
                point = cc.v2(x, y);
                pointList.push(point);
            }
        }
        return pointList;
    }

    /** 绘制
     * @param targetPointList 目标点坐标
     * @param degrees 每个维度得分(0-1)
     * @param vertexRadius 顶点圆形半径
     */
    private $draw(targetPointList: cc.Vec2[], degrees: number[]): void {
        this.view.drawMap(targetPointList, degrees, this.vertexRadius, this.lineWidth, this.fillColor, this.lineColor, this.vertexColor);
    }

}
