/*
 * @Author: bob.xiong 
 * @Date: 2021-08-23 18:12:49 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-24 10:10:15
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class DimensionBg extends cc.Graphics {

    /** 绘制背景 */
    public drawBg(targetPoins: cc.Vec2[], lineWidth: number, fillColor: cc.Color, bgLineColor: cc.Color, level: number): void {
        this.clear();

        this.lineWidth = lineWidth;

        let startPoint: cc.Vec2 = targetPoins[0];
        let point: cc.Vec2 = null;
        this.moveTo(startPoint.x, startPoint.y);
        for (let i = 1; i < targetPoins.length; i++) {
            point = targetPoins[i];
            this.lineTo(point.x, point.y);
        }
        this.fillColor = fillColor;
        this.lineTo(startPoint.x, startPoint.y);
        this.fill();

        this.strokeColor = bgLineColor;
        for (let i = 0; i < targetPoins.length; i++) {
            this.$drawBgLine(targetPoins[i]);
        }
        this.stroke();

        for (let i = 1; i <= level; i++) {
            this.$drawShape(targetPoins, i / level);
        }
    }

    private $drawShape(targetPoins: cc.Vec2[], delta: number): void {
        let startPoint: cc.Vec2 = targetPoins[0];
        let point: cc.Vec2 = null;
        this.moveTo(startPoint.x * delta, startPoint.y * delta);
        for (let i = 1; i < targetPoins.length; i++) {
            point = targetPoins[i];
            this.lineTo(point.x * delta, point.y * delta);
        }
        this.lineTo(startPoint.x * delta, startPoint.y * delta);
        this.stroke();
    }

    private $drawBgLine(point: cc.Vec2): void {
        this.moveTo(0, 0);
        this.lineTo(point.x, point.y);
    }

}
