/*
 * @Author: bob.xiong 
 * @Date: 2021-04-26 15:40:10 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-04-26 15:40:44
 */

/** spine播放参数 */
export class MSpineData {
    public animationName: string = '';
    public callback: Function = null;
    /**
     * @param animationName 
     * @param loop 
     * @param callback 
     */
    constructor(animationName: string, callback?: Function) {
        this.animationName = animationName;
        this.callback = callback;
    }
}