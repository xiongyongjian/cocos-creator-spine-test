import { MSpineData } from './MSpineData';
import { MSpineQueueData } from './MSpineQueueData';
/*
 * @Author: bob.xiong 
 * @Date: 2021-04-26 15:38:27 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-05-18 17:45:15
 */


const { ccclass, property } = cc._decorator;

@ccclass
export default class MSpine extends sp.Skeleton {

    private _queueData: MSpineQueueData = null;

    /** 播放动画 */
    public play(animationName: string, loop: boolean, callback?: Function): void {
        this.$stopAnimationQueue();
        this.paused = false;
        this.$onPlay(animationName, loop, callback);
    }

    /** 播放动画队列 */
    public playList(spineDatas: MSpineData[], completeCallback?: Function, breakCallback?: Function): void {
        let queueData: MSpineQueueData = new MSpineQueueData(spineDatas, completeCallback, breakCallback);
        this.$runAnimationQueue(queueData);
    }

    private $runAnimationQueue(queueData: MSpineQueueData): void {
        this.$stopAnimationQueue();
        this._queueData = queueData;
        queueData.isRuning = true;
        this.paused = false;
        this.$excute(queueData);
    }

    private $stopAnimationQueue(): void {
        this.paused = true;
        if (this._queueData && this._queueData.isRuning) {
            this._queueData.isRuning = false;
            if (!this._queueData.isCompleted) {
                this._queueData.breakCallback && this._queueData.breakCallback();
            }
        }
        this._queueData = null;
    }

    private $excute(queueData: MSpineQueueData): void {
        if (!queueData.isRuning) {
            return;
        }
        if (queueData.index >= queueData.spineDatas.length) {
            queueData.completeCallback && queueData.completeCallback();
            queueData.isCompleted = true;
            this.$stopAnimationQueue();
            return;
        }
        let spineData: MSpineData = queueData.spineDatas[queueData.index];
        this.$onPlay(spineData.animationName, false, () => {
            spineData.callback && spineData.callback();
            queueData.index++;
            this.$excute(queueData);
        });
    }

    /** 播放动画 */
    private $onPlay(animationName: string, loop: boolean, callback: Function): void {
        this.setCompleteListener(() => {
            if (!loop) {
                this.setCompleteListener(null);
            }
            callback && callback();
        });
        this.setAnimation(0, animationName, loop);
    }

}

