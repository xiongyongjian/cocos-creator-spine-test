import { MSpineData } from './MSpineData';
/*
 * @Author: bob.xiong 
 * @Date: 2021-04-26 15:40:32 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-04-26 16:03:20
 */


/** 动画队列数据 */
export class MSpineQueueData {
    public spineDatas: MSpineData[];
    public completeCallback: Function = null;
    public breakCallback: Function = null;
    public isRuning: boolean = false;
    public isCompleted: boolean = false;
    public index: number = 0;

    constructor(spineDatas: MSpineData[], callback?: Function, breakCallback?: Function) {
        this.spineDatas = spineDatas;
        this.completeCallback = callback;
        this.breakCallback = breakCallback;
    }
}