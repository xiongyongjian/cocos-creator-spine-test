/*
 * @Author: bob.xiong 
 * @Date: 2021-08-16 16:57:54 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-22 16:45:20
 */

import { MSlot } from "./MSlot";

/** spine 的每个部分 - 如： 头, 上衣， 裤子， 脚 */
export class MPart {

    private _slotMap: { [key: string]: MSlot } = {};

    constructor(ske: sp.Skeleton, slotNameList: string[]) {
        this._slotMap = {};
        let slot: MSlot = null;
        let slotName: string = '';
        for (let i = 0; i < slotNameList.length; i++) {
            slotName = slotNameList[i];
            slot = new MSlot(ske, slotName);
            if (slot.checkSlotCorrect()) {
                this._slotMap[slotName] = slot;
            }
        }
    }

    public changeSlotsTexture(atlas: cc.SpriteAtlas): void {
        let slot: MSlot = null;
        let spriteFrame: cc.SpriteFrame = null;

        for (let key in this._slotMap) {
            slot = this._slotMap[key];
            if (!slot) {
                console.error(`[MPart]: changeSlotsTexture 插槽为空`);
                continue
            }
            spriteFrame = atlas.getSpriteFrame(key);
            if (!spriteFrame) {
                console.error(`[MPart]: changeSlotsTexture 图集中找不到纹理 name: ${key}`);
                continue;
            }
            slot.changeTexture(spriteFrame);
        }

    }
}