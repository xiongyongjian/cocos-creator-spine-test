/*
 * @Author: bob.xiong 
 * @Date: 2021-08-16 16:56:56 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-26 17:33:15
 */

export class MSlotData {

    /** 插槽名map */
    public partSlotNamesMap: { [key: string]: string[] };
    
    /** 图集路径map */
    public atlasPathMap: { [key: string]: string };

    constructor(partSlotNamesMap: { [key: string]: string[] }, atlasPathMap: { [key: string]: string }) {
        this.partSlotNamesMap = partSlotNamesMap;
        this.atlasPathMap = atlasPathMap;
    }

}