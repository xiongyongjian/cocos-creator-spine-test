/*
 * @Author: bob.xiong 
 * @Date: 2021-08-16 14:21:09 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-26 17:44:08
 */

import { ResMgr } from "../res/ResMgr";
import { MPart } from "./MPart";
import { MSlotData } from "./MSlotData";

/** spine 插槽管理器 */
export class SlotMgr {

    private _ske: sp.Skeleton = null;

    private _partsMap: { [key: string]: MPart } = {};

    private _data: MSlotData = null;

    /** 
     * @param sketon sp.Skeleton spine对象
     * @param partSlotNamesMap map - key: partName - value: slotName[]
     * @param atlasPathMap map - key: partName - value: atlasPath 图集路径
     */
    constructor(sketon: sp.Skeleton, partSlotNamesMap: { [key: string]: string[] }, atlasPathMap: { [key: string]: string }) {
        this._ske = sketon;
        this._data = new MSlotData(partSlotNamesMap, atlasPathMap);
        this.$deepcopySkeData();
        this._initParts();
    }

    /** 初始化每个部分 */
    private _initParts(): void {
        let partConfig: string[] = null;
        let part: MPart = null;
        for (let key in this._data.partSlotNamesMap) {
            partConfig = this._data.partSlotNamesMap[key];
            if (partConfig && partConfig.length > 0) {
                part = new MPart(this._ske, partConfig);
                this._partsMap[key] = part;
            }
        }
    }

    /** 切换某个部分 */
    public async changePart(partName: string, id: number): Promise<void> {
        let atlas: cc.SpriteAtlas = await this.$loadAtlas(partName, id);
        if (atlas) {
            let part: MPart = this._partsMap[partName];
            if (!part) {
                return console.error(`[SlotMgr]: changePart 获取part失败`);
            }
            part.changeSlotsTexture(atlas);
        }
    }

    /** 深拷贝一份spine数据 */
    private $deepcopySkeData(): void {
        if (!this._ske) {
            return console.error(`[SlotMgr]: $deepcopySkeData 未初始化 this._ske 为空`);
        }
        //复制一份skeletonData，换装时防止realtime模式下影响到别的动画
        let skeData: sp.SkeletonData = this._ske.skeletonData;
        let copy: any = new sp.SkeletonData();
        cc.js.mixin(copy, skeData);
        //@ts-ignore
        copy._uuid = skeData._uuid + '_' + Date.now() + '_copy';
        let old: string = copy.name;
        let newName: string = copy.name + '_copy';
        copy.name = newName;
        copy.atlasText = copy.atlasText.replace(old, newName);
        copy.textureNames[0] = newName + '.png';
        copy.init && copy.init();
        this._ske.skeletonData = copy;
    }

    private async $loadAtlas(partName: string, skinId: number): Promise<cc.SpriteAtlas> {
        let url: string = this._data.atlasPathMap[partName] + skinId;
        if (!url) {
            console.error(`[SlotMgr]: $loadAtlas 获取图集路径失败`);
            return null;
        };
        let atlas: cc.SpriteAtlas = await ResMgr.loadSpriteAtlas(url);
        if (!atlas) {
            console.error(`[SlotMgr]: $loadAtlas 加载图集失败`);
        }
        return atlas;
    }

}