/*
 * @Author: bob.xiong 
 * @Date: 2021-08-16 15:20:34 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-26 17:37:24
 */

/** 插槽封装 */
export class MSlot {

    /** spine */
    private _sketon: sp.Skeleton = null;
    /** 插槽名 */
    private _slotName: string = '';
    /** 插槽 */
    private _slot: sp.spine.Slot = null;
    /** 纹理附件 */
    private _attachment: sp.spine.RegionAttachment | sp.spine.MeshAttachment = null;

    /**
     * @param sketon 
     * @param slotName 
     */
    constructor(sketon: sp.Skeleton, slotName: string) {
        this._sketon = sketon;
        this._slotName = slotName;
        this._slot = sketon.findSlot(slotName);
        if (!this._slot) {
            console.error(`[MSlot]: constructor 获取 Slot 失败 slotName: ${slotName}`);
            return;
        }
        this._attachment = this._slot.getAttachment() as sp.spine.RegionAttachment | sp.spine.MeshAttachment;
        if (!this._attachment) {
            console.error(`[MSlot]: constructor 获取 Attachment 失败 slotName: ${slotName}`);
            return;
        }
    }

    /** 检查插槽是否正确 */
    public checkSlotCorrect(): boolean {
        return !!this._slot && !!this._attachment;
    }

    /** 切换纹理 */
    public changeTexture(spriteFrame: cc.SpriteFrame): void {

        if (!spriteFrame) { return console.error(`[MSlot]:changeTexture 传入纹理错误`); }
        if (cc.sys.isNative) {
            this.$onNativeChangeTexture(spriteFrame);
        } else {
            this.$onWebchangeTexture(spriteFrame);
        }

        //spine如果使用了缓存模式则需要刷新缓存, 一般换装为了不影响别的动画都需要设置缓存模式为privite_cache
        this._sketon.invalidAnimationCache();
    }

    /** web环境切换纹理 */
    private $onWebchangeTexture(spriteFrame: cc.SpriteFrame): void {
        let tex2d: cc.Texture2D = spriteFrame.getTexture();
        let rect: cc.Rect = spriteFrame.getRect();
        let size: cc.Size = spriteFrame.getOriginalSize();

        if (this._attachment.path == tex2d.nativeUrl) {
            return;
        }
        this._attachment.path = tex2d.nativeUrl;

        //@ts-ignore
        let spineTexture: sp.SkeletonTexture = new sp.SkeletonTexture({ width: tex2d.width, height: tex2d.height });
        spineTexture.setRealTexture(tex2d);

        // 单张图片可以不用创建page
        let page = new sp.spine.TextureAtlasPage();
        page.name = tex2d.name;
        page.uWrap = sp.spine.TextureWrap.ClampToEdge;
        page.vWrap = sp.spine.TextureWrap.ClampToEdge;
        page.texture = spineTexture;
        page.texture.setWraps(page.uWrap, page.vWrap);
        page.width = tex2d.width;
        page.height = tex2d.height;

        let region: sp.spine.TextureAtlasRegion = this._attachment.region as sp.spine.TextureAtlasRegion;
        region.page = page;

        region.x = rect.x;
        region.y = rect.y;
        region.width = size.width;
        region.height = size.height;
        region.originalWidth = size.width;
        region.originalHeight = size.height;

        region.u = region.x / page.width;
        region.v = region.y / page.height;

        region.rotate = spriteFrame.isRotated();
        if (region.rotate) {
            region.degrees = 270;
            region.u2 = (region.x + region.height) / page.width;
            region.v2 = (region.y + region.width) / page.height;
        }
        else {
            region.degrees = 0;
            region.u2 = (region.x + region.width) / page.width;
            region.v2 = (region.y + region.height) / page.height;
        }
        region.rotate = region.degrees != 0;

        // 换图后可以通过设置x、y偏移量来对准位置（如果切图有偏差）
        let offset: cc.Vec2 = spriteFrame.getOffset();
        region.offsetX = offset.x;
        region.offsetY = offset.y;

        region.texture = spineTexture;
        region.renderObject = region;

        this._attachment.width = size.width;
        this._attachment.height = size.height;

        if (this._attachment instanceof sp.spine.MeshAttachment) {
            this._attachment.updateUVs();
        } else if (this._attachment instanceof sp.spine.RegionAttachment) {
            this._attachment.setRegion(region);
            this._attachment.updateOffset();
        }
    }

    /** native环境切换纹理 */
    private $onNativeChangeTexture(spriteFrame: cc.SpriteFrame): void {

        let tex2d: cc.Texture2D = spriteFrame.getTexture();
        if (this._attachment.path == tex2d.nativeUrl) {
            return;
        }
        this._attachment.path = tex2d.nativeUrl;

        let rect: cc.Rect = spriteFrame.getRect();
        let offset: cc.Vec2 = spriteFrame.getOffset();

        //@ts-ignore
        let jsbTex = new middleware.Texture2D();
        jsbTex.setPixelsHigh(tex2d.height);
        jsbTex.setPixelsWide(tex2d.width);
        jsbTex.setNativeTexture(tex2d.getImpl());

        //@ts-ignore
        this._sketon.replaceTexture(this._slotName, jsbTex, rect.x, rect.y, rect.width, rect.height, spriteFrame.isRotated(), offset.x, offset.y, spriteFrame.isFlipX(), spriteFrame.isFlipY());
    }

}