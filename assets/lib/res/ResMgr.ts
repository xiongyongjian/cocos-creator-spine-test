/*
 * @Author: bob.xiong 
 * @Date: 2021-08-17 14:52:07 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-18 14:34:47
 */

/** 资源状态 */
enum LOAD_STATE {
    /** 未加载 */NONE = 0,
    /** 加载中 */LOADING,
    /** 加载完成 */READY
}

/** 资源管理器 */
export class ResMgr {

    /** 动态加载资源map
     *  key: url, 
     *  value: cc.SpriteFrame 
     */
    private static _resMap: { [key: string]: cc.Asset } = {};

    /** 资源加载状态
     * key: url, 
     * value: LOAD_STATE
     */
    private static _loadStateMap: { [key: string]: LOAD_STATE } = {};

    /** 资源加载成功回调
     * key: url, 
     * value:  Array<(res: cc.Asset) => void>
     */
    private static _loadedCallback: { [key: string]: Array<(res: cc.Asset) => void> } = {};

    /** 增加完成回调 */
    private static $addCompletedCallBack(key: string, resolve: any): void {
        let callbackList: Array<(res: cc.Asset) => void> = this._loadedCallback[key];
        if (!callbackList) {
            callbackList = this._loadedCallback[key] = [];
        }
        callbackList.push((res: cc.Asset) => {
            resolve(res);
        });
    }

    /** 执行完成回调 */
    private static $runCompletedCallBack(key: string): void {
        let callbackList: Array<(res: cc.Asset) => void> = this._loadedCallback[key];
        if (!callbackList) return;
        let asset: cc.Asset = this._resMap[key];
        if (!asset) {
            console.error('ResMgr -> $runCompletedCallBack -> asset is null');
            return;
        }
        while (callbackList.length > 0) {
            let cb: (res: cc.Asset) => void = callbackList.shift();
            cb && cb(asset);
        }
    }

    /** 释放资源 
     * url: 资源路径
    */
    public static releaseAsset(url: string): void {
        let asset: cc.Asset = this._resMap[url];
        delete this._resMap[url];
        delete this._loadedCallback[url];
        this._loadStateMap[url] = LOAD_STATE.NONE;
        if (!asset) return;
        cc.assetManager.releaseAsset(asset);
        // this.checkResInCache(url);
    }

    public static releaseAll(): void {
        let asset: cc.Asset = null;
        for (let key in this._resMap) {
            asset = this._resMap[key];
            delete this._resMap[key];
            delete this._loadedCallback[key];
            this._loadStateMap[key] = LOAD_STATE.NONE;
            if (!asset) return;
            cc.assetManager.releaseAsset(asset);
        }
    }

    static async loadRes<T>(url: string): Promise<T> {
        let promise: Promise<T> = new Promise<T>((resolve, reject) => {
            let state: LOAD_STATE = this._loadStateMap[url]; // 获取这个资源的状态
            let asset: cc.Asset = this._resMap[url];

            if (state === LOAD_STATE.READY && !!asset) { // 之前已经加载
                resolve(asset as unknown as T);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;

                cc.resources.load(url, cc.Asset, (err: Error, res: any) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = res;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(res);
                    }
                })
            }
        });
        return promise;
    }

    /** 加载精灵帧
     * url: 资源路径
     */
    public static async loadSpriteFrame(url: string): Promise<cc.SpriteFrame> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as cc.SpriteFrame);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.SpriteFrame, (err: Error, spriteFrame: cc.SpriteFrame) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = spriteFrame;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(spriteFrame);
                    }
                });
            }
        });
    }

    /** 加载图集 */
    public static async loadSpriteAtlas(url: string): Promise<cc.SpriteAtlas> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as cc.SpriteAtlas);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.SpriteAtlas, (err: Error, spriteAtlas: cc.SpriteAtlas) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = spriteAtlas;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(spriteAtlas);
                    }
                });
            }
        });
    }


    /** 加载spine动画 
     * url: 资源路径
    */
    public static async loadSkeletonData(url: string): Promise<sp.SkeletonData> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as sp.SkeletonData);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, sp.SkeletonData, (err: Error, skeData: sp.SkeletonData) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = skeData;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(skeData);
                    }
                });
            }
        });
    }


    /** 加载预制体
     * url: 资源路径
    */
    public static async loadPrefab(url: string): Promise<cc.Prefab> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as cc.Prefab);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.Prefab, (err: Error, prefab: cc.Prefab) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = prefab;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(prefab);
                    }
                });
            }
        });
    }

    /** 加载字体
     * url: 资源路径
    */
    public static async loadFont(url: string): Promise<cc.Font> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as cc.Font);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.Font, (err: Error, font: cc.Font) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = font;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        font.addRef();
                        resolve(font);
                    }
                });
            }
        });
    }

    /** 加载Json
     * url: 资源路径
    */
    public static async loadJson(url: string): Promise<cc.JsonAsset> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.JsonAsset = this._resMap[url] as cc.JsonAsset;
            if (state === LOAD_STATE.READY && !!asset && !!asset.json) {
                resolve(asset as cc.JsonAsset);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this.releaseAsset(url);
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.JsonAsset, (err: Error, json: cc.JsonAsset) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = json;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(json);
                    }
                });
            }
        });
    }

    /** 加载音频
     * url: 资源路径
    */
    public static async loadAudio(url: string): Promise<cc.AudioClip> {
        return new Promise((resolve: any, reject: any) => {
            let state: LOAD_STATE = this._loadStateMap[url];
            let asset: cc.Asset = this._resMap[url];
            if (state === LOAD_STATE.READY && !!asset) {
                resolve(asset as cc.AudioClip);
            } else if (state === LOAD_STATE.LOADING) {
                ResMgr.$addCompletedCallBack(url, resolve);
            } else {
                this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.AudioClip, (err: Error, clip: cc.AudioClip) => {
                    if (err) {
                        console.error(err.message);
                        ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                        reject();
                    } else {
                        ResMgr._resMap[url] = clip;
                        ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                        ResMgr.$runCompletedCallBack(url);
                        resolve(clip);
                    }
                });
            }
        });
    }

    /** 检测是否在加载中 */
    public static checkLoading(url: string): boolean {
        return ResMgr._loadStateMap[url] == LOAD_STATE.LOADING;
    }
}