/*
 * @Author: bob.xiong 
 * @Date: 2021-08-12 16:42:09 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 14:23:17
 */

import { ResMgr } from "../../lib/res/ResMgr";
import { SlotMgr } from "../../lib/slot/SlotMgr";
import MSpine from "../../lib/spine/MSpine";
import { role } from "../../modules/role/RoleConfig";


const { ccclass, property } = cc._decorator;

@ccclass
export default class SpineTest extends cc.Component {

    @property(MSpine)
    private mspine: MSpine = null;

    private _slotMgr: SlotMgr = null;

    async start(): Promise<void> {
        let skeData: sp.SkeletonData = await ResMgr.loadSkeletonData(role.SPINE_PATH);
        if (!skeData) return console.error(`[SpineTest]: start 加载spine数据失败`);

        this.mspine.skeletonData = skeData;
        this._slotMgr = new SlotMgr(this.mspine, role.PART_SLOT_NAMES_MAP, role.ATLAS_PATH_MAP);
        this.mspine.play(role.ANI_NAMES.STANDBY_2, true);
    }

    private _index: number = 0;
    private _skinId: number = 2;

    protected onClick(): void {
        switch (this._index) {
            case 0:
                this._slotMgr.changePart(role.PART_NAMES.EAR, this._skinId);
                break;
            case 1:
                this._slotMgr.changePart(role.PART_NAMES.BODY, this._skinId);
                break;
            case 2:
                this._slotMgr.changePart(role.PART_NAMES.LEG, this._skinId);
                break;
            case 3:
                this._slotMgr.changePart(role.PART_NAMES.SHOES, this._skinId);
                break;
        }
        this._index++;

        if (this._index > 3) {
            this._index = 0;
            this._skinId == 1 ? this._skinId = 2 : this._skinId = 1;
        }
    }
}
