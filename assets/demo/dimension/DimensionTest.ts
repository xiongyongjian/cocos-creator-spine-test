/*
 * @Author: bob.xiong 
 * @Date: 2021-08-31 08:49:49 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-31 09:15:14
 */

import DimensionMap from "../../modules/dimension/DimensionMap";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DimensionTest extends cc.Component {

    @property([DimensionMap])
    dimensionMaps: DimensionMap[] = [];

    start(): void {
        this.dimensionMaps[0].init([0.1, 0.33, 0.7, 0.4, 0.2]);
        this.dimensionMaps[1].init([0.1, 0.33, 0.7, 0.4, 0.2, 0.1, 0.33, 0.7, 0.4, 0.2]);
    }

    protected onClickBtn(): void {
        this.dimensionMaps[0].init([Math.random(), Math.random(), Math.random(), Math.random(), Math.random()]);
        this.dimensionMaps[1].init([
            Math.random(), Math.random(), Math.random(), Math.random(), Math.random(),
            Math.random(), Math.random(), Math.random(), Math.random(), Math.random()
        ]);
    }

}
