/*
 * @Author: bob.xiong 
 * @Date: 2021-08-30 15:38:09 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 19:43:42
 */

import CellBase from "../../modules/cell_view/Script/CellBase";


const { ccclass, property } = cc._decorator;

@ccclass
export default class MyCell extends CellBase {

    @property(cc.Node)
    spineNode: cc.Node = null;

    @property(cc.Label)
    titleLb: cc.Label = null;

    @property(sp.SkeletonData)
    spineData: sp.SkeletonData = null;

    private _ani: sp.Skeleton = null;

    onLoad(): void {
        let aniNode: cc.Node = new cc.Node('spine');
        let ani: sp.Skeleton = aniNode.addComponent(sp.Skeleton);
        ani.skeletonData = this.spineData;
        ani.enableBatch = true;
        ani.setAnimationCacheMode(sp.Skeleton.AnimationCacheMode.PRIVATE_CACHE);
        this._ani = ani;
        aniNode.parent = this.spineNode;
        ani.setAnimation(0, '001_daiji_01', true);
    }

    /** 实现CellBase的抽象方法 *******************************************/

    protected updateCell(index: number, data: number): void {
        this.titleLb.string = `${data}`;
        // this._ani.paused = false;
    }

    protected resetCell(): void {
        this.titleLb.string = ``;
        // this._ani.paused = true;
    };

    /** 实现CellBase的抽象方法 end **************************************/

}
