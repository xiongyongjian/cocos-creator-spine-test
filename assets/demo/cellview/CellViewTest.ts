/*
 * @Author: bob.xiong 
 * @Date: 2021-08-26 09:06:02 
 * @Last Modified by: bob.xiong
 * @Last Modified time: 2021-08-30 20:29:17
 */

import CellView from "../../modules/cell_view/Script/CellView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CellViewTest extends cc.Component {

    @property(CellView)
    cellview: CellView = null;

    start(): void {
        let arr: number[] = [];
        for (let i = 1; i <= 100; i++) {
            arr.push(i);
        }
        this.cellview.init(arr);
    }

}
