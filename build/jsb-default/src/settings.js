window._CCSettings = {
    platform: "android",
    groupList: [
        "default"
    ],
    collisionMatrix: [
        [
            true
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/scene/SpineTest.fire",
    orientation: "",
    server: "",
    debug: true,
    jsList: []
};
