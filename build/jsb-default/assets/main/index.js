window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  CustomButton: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "22ccctSkOBHULGEKEfzn/fB", "CustomButton");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, menu = _a.menu, ccclass = _a.ccclass, property = _a.property, inspector = _a.inspector;
    var CustomButton = function(_super) {
      __extends(CustomButton, _super);
      function CustomButton() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.isSoundEffectButton = false;
        _this.soundEffect = null;
        _this.soundVolume = 1;
        _this._isPolygonButton = false;
        _this.polygon = [];
        return _this;
      }
      Object.defineProperty(CustomButton.prototype, "isPolygonButton", {
        get: function() {
          return this._isPolygonButton;
        },
        set: function(v) {
          0 == this.polygon.length && this.resetPolygon();
          this._isPolygonButton = v;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(CustomButton.prototype, "_resetPolygon", {
        set: function(v) {
          v && this.resetPolygon();
        },
        enumerable: false,
        configurable: true
      });
      CustomButton.prototype.resetPolygon = function() {
        var box = this.node.getBoundingBox();
        var xmax = (box.xMax - this.node.x) / this.node.scaleX;
        var xmin = (box.xMin - this.node.x) / this.node.scaleX;
        var ymax = (box.yMax - this.node.y) / this.node.scaleY;
        var ymin = (box.yMin - this.node.y) / this.node.scaleY;
        this.polygon = [ cc.v2(xmin, ymin), cc.v2(xmax, ymin), cc.v2(xmax, ymax), cc.v2(xmin, ymax) ];
      };
      CustomButton.prototype.onLoad = function() {
        this.isPolygonButton && this.node._touchListener && this.node._touchListener.setSwallowTouches(false);
      };
      CustomButton.prototype._onTouchBegan = function(event) {
        var _this = this;
        var isValid = true;
        if (this.isPolygonButton && this.polygon.length >= 3) {
          var location = event.getLocation();
          var polygon = this.polygon.map(function(v) {
            return _this.node.convertToWorldSpaceAR(v);
          });
          isValid = cc.Intersection.pointInPolygon(location, polygon);
        }
        if (isValid) {
          this.isSoundEffectButton && this.soundEffect && cc.audioEngine.play(this.soundEffect, false, this.soundVolume);
          _super.prototype._onTouchBegan.call(this, event);
        }
      };
      __decorate([ property() ], CustomButton.prototype, "isSoundEffectButton", void 0);
      __decorate([ property(cc.AudioClip) ], CustomButton.prototype, "soundEffect", void 0);
      __decorate([ property({
        slide: true,
        min: 0,
        max: 1,
        step: .1
      }) ], CustomButton.prototype, "soundVolume", void 0);
      __decorate([ property({
        animatable: false
      }) ], CustomButton.prototype, "isPolygonButton", null);
      __decorate([ property({
        animatable: false,
        visible: false
      }) ], CustomButton.prototype, "_resetPolygon", null);
      __decorate([ property({
        animatable: false
      }) ], CustomButton.prototype, "_isPolygonButton", void 0);
      __decorate([ property([ cc.Vec2 ]) ], CustomButton.prototype, "polygon", void 0);
      CustomButton = __decorate([ ccclass, inspector("packages://polygon-button/compiled/inspectors/CustomButtonInspector.js"), menu("ui-extensions/polygonButton") ], CustomButton);
      return CustomButton;
    }(cc.Button);
    exports.default = CustomButton;
    cc._RF.pop();
  }, {} ],
  DimensionBg: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "66293Y/Z3lI2oCNT7fAg3Cz", "DimensionBg");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DimensionBg = function(_super) {
      __extends(DimensionBg, _super);
      function DimensionBg() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      DimensionBg.prototype.drawBg = function(targetPoins, lineWidth, fillColor, bgLineColor, level) {
        this.clear();
        this.lineWidth = lineWidth;
        var startPoint = targetPoins[0];
        var point = null;
        this.moveTo(startPoint.x, startPoint.y);
        for (var i = 1; i < targetPoins.length; i++) {
          point = targetPoins[i];
          this.lineTo(point.x, point.y);
        }
        this.fillColor = fillColor;
        this.lineTo(startPoint.x, startPoint.y);
        this.fill();
        this.strokeColor = bgLineColor;
        for (var i = 0; i < targetPoins.length; i++) this.$drawBgLine(targetPoins[i]);
        this.stroke();
        for (var i = 1; i <= level; i++) this.$drawShape(targetPoins, i / level);
      };
      DimensionBg.prototype.$drawShape = function(targetPoins, delta) {
        var startPoint = targetPoins[0];
        var point = null;
        this.moveTo(startPoint.x * delta, startPoint.y * delta);
        for (var i = 1; i < targetPoins.length; i++) {
          point = targetPoins[i];
          this.lineTo(point.x * delta, point.y * delta);
        }
        this.lineTo(startPoint.x * delta, startPoint.y * delta);
        this.stroke();
      };
      DimensionBg.prototype.$drawBgLine = function(point) {
        this.moveTo(0, 0);
        this.lineTo(point.x, point.y);
      };
      DimensionBg = __decorate([ ccclass ], DimensionBg);
      return DimensionBg;
    }(cc.Graphics);
    exports.default = DimensionBg;
    cc._RF.pop();
  }, {} ],
  DimensionMap: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "047a4f+B8RLUYkrHV6n7cGT", "DimensionMap");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DimensionBg_1 = require("./DimensionBg");
    var DimensionView_1 = require("./DimensionView");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DimensionMap = function(_super) {
      __extends(DimensionMap, _super);
      function DimensionMap() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.bgView = null;
        _this.view = null;
        _this.pointCount = 5;
        _this.level = 5;
        _this.radius = 100;
        _this.vertexRadius = 3;
        _this.lineWidth = 3;
        _this.fillColor = null;
        _this.lineColor = null;
        _this.vertexColor = null;
        _this.bgColor = null;
        _this.bgLineColor = null;
        return _this;
      }
      DimensionMap.prototype.start = function() {
        this.init([ .4, .6, .8, .5, 1 ]);
      };
      DimensionMap.prototype.init = function(degrees) {
        if (degrees.length < this.pointCount) {
          console.error("[DimensionMap]: init \u7ed8\u5236\u7ef4\u5ea6\u56fe\u53c2\u6570\u9519\u8bef");
          var len = degrees.length;
          for (var i = len; i < 5; i++) degrees.push(0);
        }
        var targetPoints = this.$generatePoints();
        this.$draw(this.$generatePoints(), degrees);
        this.bgView.drawBg(targetPoints, this.lineWidth, this.bgColor, this.bgLineColor, this.level);
      };
      DimensionMap.prototype.$generatePoints = function() {
        var pointList = [];
        var r = this.radius;
        var count = this.pointCount;
        var deltaAngle = 360 / count;
        var angle = 0;
        var x, y;
        var point;
        if (count % 2 == 0) {
          angle = .5 * deltaAngle;
          for (var i = 0; i < count; i++) {
            0 != i && (angle += deltaAngle);
            x = Math.sin(angle * Math.PI / 180) * r;
            y = Math.cos(angle * Math.PI / 180) * r;
            point = cc.v2(x, y);
            pointList.push(point);
          }
        } else {
          angle = 0;
          for (var i = 0; i < count; i++) {
            0 != i && (angle += deltaAngle);
            x = Math.sin(angle * Math.PI / 180) * r;
            y = Math.cos(angle * Math.PI / 180) * r;
            console.log(angle);
            point = cc.v2(x, y);
            pointList.push(point);
          }
        }
        return pointList;
      };
      DimensionMap.prototype.$draw = function(targetPointList, degrees) {
        this.view.drawMap(targetPointList, degrees, this.vertexRadius, this.lineWidth, this.fillColor, this.lineColor, this.vertexColor);
      };
      __decorate([ property(DimensionBg_1.default) ], DimensionMap.prototype, "bgView", void 0);
      __decorate([ property(DimensionView_1.default) ], DimensionMap.prototype, "view", void 0);
      __decorate([ property({
        type: cc.Integer,
        displayName: "\u7ef4\u5ea6\u6570"
      }) ], DimensionMap.prototype, "pointCount", void 0);
      __decorate([ property({
        type: cc.Integer,
        displayName: "\u6bcf\u4e2a\u7ef4\u5ea6\u7b49\u7ea7\u6570"
      }) ], DimensionMap.prototype, "level", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u7ef4\u5ea6\u534a\u5f84"
      }) ], DimensionMap.prototype, "radius", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u9876\u70b9\u534a\u5f84"
      }) ], DimensionMap.prototype, "vertexRadius", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u753b\u7ebf\u5bbd\u5ea6"
      }) ], DimensionMap.prototype, "lineWidth", void 0);
      __decorate([ property(cc.Color) ], DimensionMap.prototype, "fillColor", void 0);
      __decorate([ property(cc.Color) ], DimensionMap.prototype, "lineColor", void 0);
      __decorate([ property(cc.Color) ], DimensionMap.prototype, "vertexColor", void 0);
      __decorate([ property(cc.Color) ], DimensionMap.prototype, "bgColor", void 0);
      __decorate([ property(cc.Color) ], DimensionMap.prototype, "bgLineColor", void 0);
      DimensionMap = __decorate([ ccclass ], DimensionMap);
      return DimensionMap;
    }(cc.Component);
    exports.default = DimensionMap;
    cc._RF.pop();
  }, {
    "./DimensionBg": "DimensionBg",
    "./DimensionView": "DimensionView"
  } ],
  DimensionView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "21d52L+1Z9DApOBgPN614ez", "DimensionView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DimensionView = function(_super) {
      __extends(DimensionView, _super);
      function DimensionView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._targetPoins = [];
        _this._degrees = [];
        _this._vertexRadius = 2;
        _this._lineWidth = 2;
        _this._lineColor = null;
        _this._vertexColor = null;
        _this._tweenTime = 3;
        return _this;
      }
      DimensionView.prototype.drawMap = function(targetPoins, degrees, vertexRadius, lineWidth, fillColor, lineColor, vertexColor) {
        if (targetPoins.length < 3) return console.error("[DimensionView]: \u7ed8\u5236\u7ef4\u5ea6\u56fe\u53c2\u6570\u9519\u8bef \u7ef4\u5ea6\u6570\u5c0f\u4e8e3");
        this._targetPoins = targetPoins;
        this._degrees = degrees;
        this._vertexRadius = vertexRadius;
        this._lineWidth = lineWidth;
        this.fillColor = fillColor;
        this._lineColor = lineColor;
        this._vertexColor = vertexColor;
        var degree = 0;
        var point = null;
        for (var i = 0; i < targetPoins.length; i++) {
          point = targetPoins[i];
          degree = degrees[i] || 0;
          degree > .99 ? degree = .99 : null;
          point.x *= degree;
          point.y *= degree;
        }
        this.$drawAnimation();
      };
      DimensionView.prototype.$drawAnimation = function() {
        var _this = this;
        cc.Tween.stopAllByTarget(this.node);
        cc.tween(this.node).set({
          opacity: 200
        }).to(this._tweenTime, {
          opacity: 255
        }, {
          progress: function(start, end, current, ratio) {
            _this.$draw(_this._targetPoins, ratio);
            return start + (end - start) * ratio;
          },
          easing: "expoOut"
        }).start();
      };
      DimensionView.prototype.$draw = function(points, ratio) {
        this.clear();
        var startPoint = points[0];
        var point = null;
        this.moveTo(startPoint.x * ratio, startPoint.y * ratio);
        for (var i = 1; i < points.length; i++) {
          point = points[i];
          this.lineTo(point.x * ratio, point.y * ratio);
        }
        this.lineTo(startPoint.x * ratio, startPoint.y * ratio);
        this.fill();
        this.strokeColor = this._lineColor;
        this.stroke();
        this.$drawVertexs(points, ratio);
      };
      DimensionView.prototype.$drawVertexs = function(points, ratio) {
        var point = null;
        for (var i = 0; i < points.length; i++) {
          point = points[i];
          this.circle(point.x * ratio, point.y * ratio, this._vertexRadius);
          this.strokeColor = cc.Color.GRAY;
          this.stroke();
        }
      };
      DimensionView = __decorate([ ccclass ], DimensionView);
      return DimensionView;
    }(cc.Graphics);
    exports.default = DimensionView;
    cc._RF.pop();
  }, {} ],
  ListDataBase: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c0f45toKM9E+4rZoMTOn5Kt", "ListDataBase");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ListConfig = void 0;
    var ListConfig = function() {
      function ListConfig(top, bottom, left, right, spacingX, spacingY, rowCount, layers) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.spacingX = spacingX;
        this.spacingY = spacingY;
        this.rowCount = rowCount;
        this.layers = layers;
      }
      return ListConfig;
    }();
    exports.ListConfig = ListConfig;
    cc._RF.pop();
  }, {} ],
  ListItemBase: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "03f101n2wFHeb1J+cgOXohr", "ListItemBase");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ListItemBase = function(_super) {
      __extends(ListItemBase, _super);
      function ListItemBase() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.layers = [];
        _this.lb = null;
        _this._layerPositions = [];
        _this._index = -1;
        _this._config = null;
        return _this;
      }
      ListItemBase.prototype.init = function(config) {
        this._config = config;
        if (0 == this.layers.length || 0 == config.layers.length) return;
        var itemLayer = null;
        var contentLayer = null;
        var gPos = null;
        for (var i = 0; i < this.layers.length; i++) {
          itemLayer = this.layers[i];
          itemLayer.getComponent(cc.Widget) && itemLayer.removeComponent(cc.Widget);
          this._layerPositions.push(itemLayer.position);
          contentLayer = i < config.layers.length ? config.layers[i] : config.layers[config.layers.length - 1];
          gPos = this.node.convertToWorldSpaceAR(itemLayer.position);
          itemLayer.parent = contentLayer;
          itemLayer.position = this.node.convertToNodeSpaceAR(gPos);
        }
      };
      ListItemBase.prototype.updateOffset = function(index) {
        if (this._index == index) return;
        index++;
        if (!this._config) return console.error("[ListItemBase]: updateOffset item \u672a\u521d\u59cb\u5316");
        var config = this._config;
        var indexY = Math.ceil(index / config.rowCount);
        var indexX = index % config.rowCount;
        0 == indexX ? indexX = config.rowCount : null;
        var x = config.left + (this.node.width + config.spacingX) * (indexX - 1);
        var y = config.top + (this.node.height + config.spacingY) * (indexY - 1);
        this.node.x = x;
        this.node.y = -y;
        this.$updateLayers();
      };
      ListItemBase.prototype.updateData = function(index, dataList) {
        if (this._index == index) return;
        var data = dataList[index];
        this.lb.string = "" + data;
      };
      ListItemBase.prototype.$updateLayers = function() {
        if (this.layers.length > 0 && this._config.layers.length > 0) {
          var itemLayer = null;
          var layerPosition = null;
          var gPos = null;
          for (var i = 0; i < this.layers.length; i++) {
            itemLayer = this.layers[i];
            layerPosition = this._layerPositions[i];
            gPos = this.node.convertToWorldSpaceAR(layerPosition);
            itemLayer.position = this.node.parent.convertToNodeSpaceAR(gPos);
          }
        }
      };
      __decorate([ property({
        type: [ cc.Node ],
        displayName: "\u5206\u5c42"
      }) ], ListItemBase.prototype, "layers", void 0);
      __decorate([ property(cc.Label) ], ListItemBase.prototype, "lb", void 0);
      ListItemBase = __decorate([ ccclass ], ListItemBase);
      return ListItemBase;
    }(cc.Component);
    exports.default = ListItemBase;
    cc._RF.pop();
  }, {} ],
  ListTest: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dd01cyg95JDSr9mSc3+uzSj", "ListTest");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ListTest = function(_super) {
      __extends(ListTest, _super);
      function ListTest() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      ListTest = __decorate([ ccclass ], ListTest);
      return ListTest;
    }(cc.Component);
    exports.default = ListTest;
    cc._RF.pop();
  }, {} ],
  ListView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8655cZM/nlNq7wnoaXiA1xL", "ListView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ListDataBase_1 = require("./data/ListDataBase");
    var ListItemBase_1 = require("./ListItemBase");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ListView = function(_super) {
      __extends(ListView, _super);
      function ListView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.top = 10;
        _this.bottom = 10;
        _this.left = 10;
        _this.right = 10;
        _this.spacingX = 20;
        _this.spacingY = 20;
        _this.rowCount = 3;
        _this.layerCount = 0;
        _this.item = null;
        _this._layers = [];
        _this._config = null;
        _this._itemWidth = 0;
        _this._itemHeight = 0;
        return _this;
      }
      ListView.prototype.onLoad = function() {
        this.$initLayers();
        this._config = new ListDataBase_1.ListConfig(this.top, this.bottom, this.left, this.right, this.spacingX, this.spacingY, this.rowCount, this._layers);
        if (!this.item || !this.item.data) return console.error("[ListView]: onload \u9884\u5236\u4f53\u8bbe\u7f6e\u9519\u8bef");
        this._itemWidth = this.item.data.width;
        this._itemHeight = this.item.data.height;
        this.node.width = this._itemWidth * this.rowCount + this.spacingX * (this.rowCount - 1) + this.left + this.right;
        this.content.width = this.node.width;
      };
      ListView.prototype.start = function() {
        var arr = [];
        var i = 0;
        for (i = 1; i <= 20; i++) arr.push(i);
        this.initList(arr);
      };
      ListView.prototype.initList = function(dataList, index) {
        void 0 === index && (index = 0);
        if (!dataList || 0 == dataList.length) return console.error("[ListView]: initList \u521d\u59cb\u5316\u5217\u8868\u4f20\u5165\u6570\u636e\u9519\u8bef");
        var itemNode = null;
        for (var i = 0; i < dataList.length; i++) {
          itemNode = cc.instantiate(this.item);
          itemNode.x = itemNode.y = -500;
          itemNode.parent = this.content;
          itemNode.getComponent(ListItemBase_1.default).init(this._config);
          itemNode.getComponent(ListItemBase_1.default).updateOffset(i);
          itemNode.getComponent(ListItemBase_1.default).updateData(i, dataList);
        }
        var len = dataList.length;
        var yCount = Math.ceil(len / this.rowCount);
        this.content.height = this._itemHeight * yCount + this.spacingY * (yCount - 1) + this.top + this.bottom;
      };
      ListView.prototype.$initLayers = function() {
        if (this.layerCount > 0) {
          var layer = void 0;
          for (var i = 0; i < this.layerCount; i++) {
            layer = new cc.Node("layer_" + (i + 1));
            layer.parent = this.content;
            layer.x = layer.y = 0;
            layer.width = layer.height = 0;
            layer.zIndex = i + 1;
            this._layers.push(layer);
          }
        }
      };
      __decorate([ property(cc.Float) ], ListView.prototype, "top", void 0);
      __decorate([ property(cc.Float) ], ListView.prototype, "bottom", void 0);
      __decorate([ property(cc.Float) ], ListView.prototype, "left", void 0);
      __decorate([ property(cc.Float) ], ListView.prototype, "right", void 0);
      __decorate([ property(cc.Float) ], ListView.prototype, "spacingX", void 0);
      __decorate([ property(cc.Float) ], ListView.prototype, "spacingY", void 0);
      __decorate([ property({
        type: cc.Integer,
        displayName: "\u5217\u6570"
      }) ], ListView.prototype, "rowCount", void 0);
      __decorate([ property({
        type: cc.Integer,
        displayName: "\u5206\u5c42"
      }) ], ListView.prototype, "layerCount", void 0);
      __decorate([ property(cc.Prefab) ], ListView.prototype, "item", void 0);
      ListView = __decorate([ ccclass ], ListView);
      return ListView;
    }(cc.ScrollView);
    exports.default = ListView;
    cc._RF.pop();
  }, {
    "./ListItemBase": "ListItemBase",
    "./data/ListDataBase": "ListDataBase"
  } ],
  MPart: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c736cahA8hPC5a4X373hSTn", "MPart");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.MPart = void 0;
    var MSlot_1 = require("./MSlot");
    var MPart = function() {
      function MPart(ske, slotNameList) {
        this._ske = null;
        this._slotMap = {};
        this._ske = ske;
        this._slotMap = {};
        var slot = null;
        var slotName = "";
        for (var i = 0; i < slotNameList.length; i++) {
          slotName = slotNameList[i];
          slot = new MSlot_1.MSlot(ske, slotName);
          slot.checkSlotCorrect() && (this._slotMap[slotName] = slot);
        }
      }
      MPart.prototype.changeSlotsTexture = function(atlas) {
        var slot = null;
        var spriteFrame = null;
        for (var key in this._slotMap) {
          slot = this._slotMap[key];
          if (!slot) {
            console.error("[MPart]: changeSlotsTexture \u63d2\u69fd\u4e3a\u7a7a");
            continue;
          }
          spriteFrame = atlas.getSpriteFrame(key);
          if (!spriteFrame) {
            console.error("[MPart]: changeSlotsTexture \u56fe\u96c6\u4e2d\u627e\u4e0d\u5230\u7eb9\u7406 name: " + key);
            continue;
          }
          slot.changeTexture(spriteFrame);
        }
      };
      return MPart;
    }();
    exports.MPart = MPart;
    cc._RF.pop();
  }, {
    "./MSlot": "MSlot"
  } ],
  MSlotData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0ff69tybfdMYbUIKhRATqiC", "MSlotData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.MSlotData = void 0;
    var MSlotData = function() {
      function MSlotData(partSlotNamesMap, atlasPathMap) {
        this.partSlotNamesMap = partSlotNamesMap;
        this.atlasPathMap = atlasPathMap;
      }
      return MSlotData;
    }();
    exports.MSlotData = MSlotData;
    cc._RF.pop();
  }, {} ],
  MSlot: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "72af3X79tNNmrBh0dhw2C5s", "MSlot");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.MSlot = void 0;
    var MSlot = function() {
      function MSlot(sketon, slotName) {
        this.isMesh = false;
        this._sketon = null;
        this._slotName = "";
        this._slot = null;
        this._attachment = null;
        this._sketon = sketon;
        this._slotName = slotName;
        this._slot = sketon.findSlot(slotName);
        if (!this._slot) {
          console.error("[MSlot]: constructor \u83b7\u53d6 Slot \u5931\u8d25 slotName: " + slotName);
          return;
        }
        this._attachment = this._slot.getAttachment();
        if (!this._attachment) {
          console.error("[MSlot]: constructor \u83b7\u53d6 Attachment \u5931\u8d25 slotName: " + slotName);
          return;
        }
        this.isMesh = this._attachment instanceof sp.spine.MeshAttachment;
      }
      MSlot.prototype.checkSlotCorrect = function() {
        return !!this._slot && !!this._attachment;
      };
      MSlot.prototype.changeTexture = function(spriteFrame) {
        if (!spriteFrame) return console.error("[MSlot]:changeTexture \u4f20\u5165\u7eb9\u7406\u9519\u8bef");
        cc.sys.isNative ? this.$onNativeChangeTexture(spriteFrame) : this.$onWebchangeTexture(spriteFrame);
        this._sketon.invalidAnimationCache();
      };
      MSlot.prototype.$onWebchangeTexture = function(spriteFrame) {
        var tex2d = spriteFrame.getTexture();
        var rect = spriteFrame.getRect();
        var size = spriteFrame.getOriginalSize();
        if (this._attachment.path == tex2d.nativeUrl) return;
        this._attachment.path = tex2d.nativeUrl;
        var spineTexture = new sp.SkeletonTexture({
          width: tex2d.width,
          height: tex2d.height
        });
        spineTexture.setRealTexture(tex2d);
        var page = new sp.spine.TextureAtlasPage();
        page.name = tex2d.name;
        page.uWrap = sp.spine.TextureWrap.ClampToEdge;
        page.vWrap = sp.spine.TextureWrap.ClampToEdge;
        page.texture = spineTexture;
        page.texture.setWraps(page.uWrap, page.vWrap);
        page.width = tex2d.width;
        page.height = tex2d.height;
        var region = this._attachment.region;
        region.page = page;
        region.x = rect.x;
        region.y = rect.y;
        region.width = size.width;
        region.height = size.height;
        region.originalWidth = size.width;
        region.originalHeight = size.height;
        region.u = region.x / page.width;
        region.v = region.y / page.height;
        region.rotate = spriteFrame.isRotated();
        if (region.rotate) {
          region.degrees = 270;
          region.u2 = (region.x + region.height) / page.width;
          region.v2 = (region.y + region.width) / page.height;
        } else {
          region.degrees = 0;
          region.u2 = (region.x + region.width) / page.width;
          region.v2 = (region.y + region.height) / page.height;
        }
        region.rotate = 0 != region.degrees;
        var offset = spriteFrame.getOffset();
        region.offsetX = offset.x;
        region.offsetY = offset.y;
        region.texture = spineTexture;
        region.renderObject = region;
        this._attachment.width = size.width;
        this._attachment.height = size.height;
        if (this._attachment instanceof sp.spine.MeshAttachment) this._attachment.updateUVs(); else if (this._attachment instanceof sp.spine.RegionAttachment) {
          this._attachment.setRegion(region);
          this._attachment.updateOffset();
        }
      };
      MSlot.prototype.$onNativeChangeTexture = function(spriteFrame) {
        var tex2d = spriteFrame.getTexture();
        if (this._attachment.path == tex2d.nativeUrl) return;
        this._attachment.path = tex2d.nativeUrl;
        var rect = spriteFrame.getRect();
        var offset = spriteFrame.getOffset();
        var jsbTex = new middleware.Texture2D();
        jsbTex.setPixelsHigh(tex2d.height);
        jsbTex.setPixelsWide(tex2d.width);
        jsbTex.setNativeTexture(tex2d.getImpl());
        this._sketon.replaceTexture(this._slotName, jsbTex, rect.x, rect.y, rect.width, rect.height, spriteFrame.isRotated(), offset.x, offset.y, spriteFrame.isFlipX(), spriteFrame.isFlipY());
      };
      return MSlot;
    }();
    exports.MSlot = MSlot;
    cc._RF.pop();
  }, {} ],
  MSpineData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9af72ZR3fpMQ51hXmNPVDCE", "MSpineData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.MSpineData = void 0;
    var MSpineData = function() {
      function MSpineData(animationName, callback) {
        this.animationName = "";
        this.callback = null;
        this.animationName = animationName;
        this.callback = callback;
      }
      return MSpineData;
    }();
    exports.MSpineData = MSpineData;
    cc._RF.pop();
  }, {} ],
  MSpineQueueData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "be08aqcJk1BhpUaGCbsumrR", "MSpineQueueData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.MSpineQueueData = void 0;
    var MSpineQueueData = function() {
      function MSpineQueueData(spineDatas, callback, breakCallback) {
        this.completeCallback = null;
        this.breakCallback = null;
        this.isRuning = false;
        this.isCompleted = false;
        this.index = 0;
        this.spineDatas = spineDatas;
        this.completeCallback = callback;
        this.breakCallback = breakCallback;
      }
      return MSpineQueueData;
    }();
    exports.MSpineQueueData = MSpineQueueData;
    cc._RF.pop();
  }, {} ],
  MSpine: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f4f6cqGBvxK1aP2s3XG2LXS", "MSpine");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MSpineQueueData_1 = require("./MSpineQueueData");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MSpine = function(_super) {
      __extends(MSpine, _super);
      function MSpine() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._queueData = null;
        return _this;
      }
      MSpine.prototype.play = function(animationName, loop, callback) {
        this.$stopAnimationQueue();
        this.paused = false;
        this.$onPlay(animationName, loop, callback);
      };
      MSpine.prototype.playList = function(spineDatas, completeCallback, breakCallback) {
        var queueData = new MSpineQueueData_1.MSpineQueueData(spineDatas, completeCallback, breakCallback);
        this.$runAnimationQueue(queueData);
      };
      MSpine.prototype.$runAnimationQueue = function(queueData) {
        this.$stopAnimationQueue();
        this._queueData = queueData;
        queueData.isRuning = true;
        this.paused = false;
        this.$excute(queueData);
      };
      MSpine.prototype.$stopAnimationQueue = function() {
        this.paused = true;
        if (this._queueData && this._queueData.isRuning) {
          this._queueData.isRuning = false;
          this._queueData.isCompleted || this._queueData.breakCallback && this._queueData.breakCallback();
        }
        this._queueData = null;
      };
      MSpine.prototype.$excute = function(queueData) {
        var _this = this;
        if (!queueData.isRuning) return;
        if (queueData.index >= queueData.spineDatas.length) {
          queueData.completeCallback && queueData.completeCallback();
          queueData.isCompleted = true;
          this.$stopAnimationQueue();
          return;
        }
        var spineData = queueData.spineDatas[queueData.index];
        this.$onPlay(spineData.animationName, false, function() {
          spineData.callback && spineData.callback();
          queueData.index++;
          _this.$excute(queueData);
        });
      };
      MSpine.prototype.$onPlay = function(animationName, loop, callback) {
        var _this = this;
        this.setCompleteListener(function() {
          loop || _this.setCompleteListener(null);
          callback && callback();
        });
        this.setAnimation(0, animationName, loop);
      };
      MSpine = __decorate([ ccclass ], MSpine);
      return MSpine;
    }(sp.Skeleton);
    exports.default = MSpine;
    cc._RF.pop();
  }, {
    "./MSpineQueueData": "MSpineQueueData"
  } ],
  ResMgr: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f643eampDdDg5RUHhwHQRIy", "ResMgr");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ResMgr = void 0;
    var LOAD_STATE;
    (function(LOAD_STATE) {
      LOAD_STATE[LOAD_STATE["NONE"] = 0] = "NONE";
      LOAD_STATE[LOAD_STATE["LOADING"] = 1] = "LOADING";
      LOAD_STATE[LOAD_STATE["READY"] = 2] = "READY";
    })(LOAD_STATE || (LOAD_STATE = {}));
    var ResMgr = function() {
      function ResMgr() {}
      ResMgr.$addCompletedCallBack = function(key, resolve) {
        var callbackList = this._loadedCallback[key];
        callbackList || (callbackList = this._loadedCallback[key] = []);
        callbackList.push(function(res) {
          resolve(res);
        });
      };
      ResMgr.$runCompletedCallBack = function(key) {
        var callbackList = this._loadedCallback[key];
        if (!callbackList) return;
        var asset = this._resMap[key];
        if (!asset) {
          console.error("ResMgr -> $runCompletedCallBack -> asset is null");
          return;
        }
        while (callbackList.length > 0) {
          var cb = callbackList.shift();
          cb && cb(asset);
        }
      };
      ResMgr.releaseAsset = function(url) {
        var asset = this._resMap[url];
        delete this._resMap[url];
        delete this._loadedCallback[url];
        this._loadStateMap[url] = LOAD_STATE.NONE;
        if (!asset) return;
        cc.assetManager.releaseAsset(asset);
      };
      ResMgr.releaseAll = function() {
        var asset = null;
        for (var key in this._resMap) {
          asset = this._resMap[key];
          delete this._resMap[key];
          delete this._loadedCallback[key];
          this._loadStateMap[key] = LOAD_STATE.NONE;
          if (!asset) return;
          cc.assetManager.releaseAsset(asset);
        }
      };
      ResMgr.loadRes = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var promise;
          var _this = this;
          return __generator(this, function(_a) {
            promise = new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.Asset, function(err, res) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = res;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(res);
                  }
                });
              } else resolve(asset);
            });
            return [ 2, promise ];
          });
        });
      };
      ResMgr.loadSpriteFrame = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.SpriteFrame, function(err, spriteFrame) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = spriteFrame;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(spriteFrame);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadSpriteAtlas = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.SpriteAtlas, function(err, spriteAtlas) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = spriteAtlas;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(spriteAtlas);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadSkeletonData = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, sp.SkeletonData, function(err, skeData) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = skeData;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(skeData);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadPrefab = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.Prefab, function(err, prefab) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = prefab;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(prefab);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadFont = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.Font, function(err, font) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = font;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    font.addRef();
                    resolve(font);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadJson = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset || !asset.json) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this.releaseAsset(url);
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.JsonAsset, function(err, json) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = json;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(json);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.loadAudio = function(url) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              var state = _this._loadStateMap[url];
              var asset = _this._resMap[url];
              if (state !== LOAD_STATE.READY || !asset) if (state === LOAD_STATE.LOADING) ResMgr.$addCompletedCallBack(url, resolve); else {
                _this._loadStateMap[url] = LOAD_STATE.LOADING;
                cc.resources.load(url, cc.AudioClip, function(err, clip) {
                  if (err) {
                    console.error(err.message);
                    ResMgr._loadStateMap[url] = LOAD_STATE.NONE;
                    reject();
                  } else {
                    ResMgr._resMap[url] = clip;
                    ResMgr._loadStateMap[url] = LOAD_STATE.READY;
                    ResMgr.$runCompletedCallBack(url);
                    resolve(clip);
                  }
                });
              } else resolve(asset);
            }) ];
          });
        });
      };
      ResMgr.checkLoading = function(url) {
        return ResMgr._loadStateMap[url] == LOAD_STATE.LOADING;
      };
      ResMgr._resMap = {};
      ResMgr._loadStateMap = {};
      ResMgr._loadedCallback = {};
      return ResMgr;
    }();
    exports.ResMgr = ResMgr;
    cc._RF.pop();
  }, {} ],
  RoleConfig: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "aaecbodKsJOXaRQC9dUWrNK", "RoleConfig");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.role = void 0;
    var role;
    (function(role) {
      role.SPINE_PATH = "spine/racoon-normal";
      var ANI_NAMES;
      (function(ANI_NAMES) {
        ANI_NAMES["STANDBY"] = "001_daiji_01";
        ANI_NAMES["STANDBY_2"] = "001_daiji_02";
        ANI_NAMES["JUMP_1"] = "001_jump_01";
        ANI_NAMES["JUMP_2"] = "001_jump_01_L";
        ANI_NAMES["JUMP_3"] = "001_jump_01_R";
        ANI_NAMES["JUMP_4"] = "001_jump_02_L";
        ANI_NAMES["JUMP_5"] = "001_jump_02_R";
        ANI_NAMES["JUMP_6"] = "001_jump_snow";
        ANI_NAMES["LOSE"] = "001_lose";
        ANI_NAMES["WIN"] = "001_win";
        ANI_NAMES["WIN_2"] = "001_win_02";
        ANI_NAMES["WIN_3"] = "001_win_03";
      })(ANI_NAMES = role.ANI_NAMES || (role.ANI_NAMES = {}));
      var PART_NAMES;
      (function(PART_NAMES) {
        PART_NAMES["BODY"] = "body";
        PART_NAMES["EAR"] = "ear";
        PART_NAMES["LEG"] = "leg";
        PART_NAMES["SHOES"] = "shoes";
      })(PART_NAMES = role.PART_NAMES || (role.PART_NAMES = {}));
      role.PART_SLOT_NAMES_MAP = {
        body: [ "racoon_arm_L", "racoon_arm_L_out", "racoon_arm_R", "racoon_arm_R_out", "racoon_body", "racoon_weiba" ],
        ear: [ "racoon_ear_L", "racoon_ear_R" ],
        leg: [ "racoon_leg_L", "racoon_leg_R" ],
        shoes: [ "racoon_foot_L", "racoon_foot_R" ]
      };
      role.ATLAS_PATH_MAP = {
        body: "skin/body_",
        ear: "skin/ear_",
        leg: "skin/leg_",
        shoes: "skin/shoes_"
      };
    })(role = exports.role || (exports.role = {}));
    cc._RF.pop();
  }, {} ],
  SlotMgr: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6da11d9tCpIrZUQvXyL+tpN", "SlotMgr");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SlotMgr = void 0;
    var ResMgr_1 = require("../res/ResMgr");
    var MPart_1 = require("./MPart");
    var MSlotData_1 = require("./MSlotData");
    var SlotMgr = function() {
      function SlotMgr(sketon, partSlotNamesMap, atlasPathMap) {
        this._ske = null;
        this._partsMap = {};
        this._data = null;
        this._ske = sketon;
        this._data = new MSlotData_1.MSlotData(partSlotNamesMap, atlasPathMap);
        this.$deepcopySkeData();
        this._initParts();
      }
      SlotMgr.prototype._initParts = function() {
        var partConfig = null;
        var part = null;
        for (var key in this._data.partSlotNamesMap) {
          partConfig = this._data.partSlotNamesMap[key];
          if (partConfig && partConfig.length > 0) {
            part = new MPart_1.MPart(this._ske, partConfig);
            this._partsMap[key] = part;
          }
        }
      };
      SlotMgr.prototype.changePart = function(partName, id) {
        return __awaiter(this, void 0, Promise, function() {
          var atlas, part;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.$loadAtlas(partName, id) ];

             case 1:
              atlas = _a.sent();
              if (atlas) {
                part = this._partsMap[partName];
                if (!part) return [ 2, console.error("[SlotMgr]: changePart \u83b7\u53d6part\u5931\u8d25") ];
                part.changeSlotsTexture(atlas);
              }
              return [ 2 ];
            }
          });
        });
      };
      SlotMgr.prototype.$deepcopySkeData = function() {
        if (!this._ske) return console.error("[SlotMgr]: $deepcopySkeData \u672a\u521d\u59cb\u5316 this._ske \u4e3a\u7a7a");
        var skeData = this._ske.skeletonData;
        var copy = new sp.SkeletonData();
        cc.js.mixin(copy, skeData);
        copy._uuid = skeData._uuid + "_" + Date.now() + "_copy";
        var old = copy.name;
        var newName = copy.name + "_copy";
        copy.name = newName;
        copy.atlasText = copy.atlasText.replace(old, newName);
        copy.textureNames[0] = newName + ".png";
        copy.init && copy.init();
        this._ske.skeletonData = copy;
      };
      SlotMgr.prototype.$loadAtlas = function(partName, skinId) {
        return __awaiter(this, void 0, Promise, function() {
          var url, atlas;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              url = this._data.atlasPathMap[partName] + skinId;
              if (!url) {
                console.error("[SlotMgr]: $loadAtlas \u83b7\u53d6\u56fe\u96c6\u8def\u5f84\u5931\u8d25");
                return [ 2, null ];
              }
              return [ 4, ResMgr_1.ResMgr.loadSpriteAtlas(url) ];

             case 1:
              atlas = _a.sent();
              atlas || console.error("[SlotMgr]: $loadAtlas \u52a0\u8f7d\u56fe\u96c6\u5931\u8d25");
              return [ 2, atlas ];
            }
          });
        });
      };
      return SlotMgr;
    }();
    exports.SlotMgr = SlotMgr;
    cc._RF.pop();
  }, {
    "../res/ResMgr": "ResMgr",
    "./MPart": "MPart",
    "./MSlotData": "MSlotData"
  } ],
  SpineTest: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5710fzctKxLFapjCaFuddAh", "SpineTest");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ResMgr_1 = require("./lib/res/ResMgr");
    var SlotMgr_1 = require("./lib/slot/SlotMgr");
    var MSpine_1 = require("./lib/spine/MSpine");
    var RoleConfig_1 = require("./modules/role/RoleConfig");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var SpineTest = function(_super) {
      __extends(SpineTest, _super);
      function SpineTest() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.mspine = null;
        _this._slotMgr = null;
        _this._index = 0;
        _this._skinId = 2;
        return _this;
      }
      SpineTest.prototype.start = function() {
        return __awaiter(this, void 0, Promise, function() {
          var skeData;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, ResMgr_1.ResMgr.loadSkeletonData(RoleConfig_1.role.SPINE_PATH) ];

             case 1:
              skeData = _a.sent();
              if (!skeData) return [ 2, console.error("[SpineTest]: start \u52a0\u8f7dspine\u6570\u636e\u5931\u8d25") ];
              this.mspine.skeletonData = skeData;
              this._slotMgr = new SlotMgr_1.SlotMgr(this.mspine, RoleConfig_1.role.PART_SLOT_NAMES_MAP, RoleConfig_1.role.ATLAS_PATH_MAP);
              this.mspine.play(RoleConfig_1.role.ANI_NAMES.STANDBY_2, true);
              return [ 2 ];
            }
          });
        });
      };
      SpineTest.prototype.onClick = function() {
        switch (this._index) {
         case 0:
          this._slotMgr.changePart(RoleConfig_1.role.PART_NAMES.EAR, this._skinId);
          break;

         case 1:
          this._slotMgr.changePart(RoleConfig_1.role.PART_NAMES.BODY, this._skinId);
          break;

         case 2:
          this._slotMgr.changePart(RoleConfig_1.role.PART_NAMES.LEG, this._skinId);
          break;

         case 3:
          this._slotMgr.changePart(RoleConfig_1.role.PART_NAMES.SHOES, this._skinId);
        }
        this._index++;
        if (this._index > 3) {
          this._index = 0;
          1 == this._skinId ? this._skinId = 2 : this._skinId = 1;
        }
      };
      __decorate([ property(MSpine_1.default) ], SpineTest.prototype, "mspine", void 0);
      SpineTest = __decorate([ ccclass ], SpineTest);
      return SpineTest;
    }(cc.Component);
    exports.default = SpineTest;
    cc._RF.pop();
  }, {
    "./lib/res/ResMgr": "ResMgr",
    "./lib/slot/SlotMgr": "SlotMgr",
    "./lib/spine/MSpine": "MSpine",
    "./modules/role/RoleConfig": "RoleConfig"
  } ]
}, {}, [ "CustomButton", "ListTest", "SpineTest", "ResMgr", "MPart", "MSlot", "MSlotData", "SlotMgr", "MSpine", "MSpineData", "MSpineQueueData", "DimensionBg", "DimensionMap", "DimensionView", "ListItemBase", "ListView", "ListDataBase", "RoleConfig" ]);